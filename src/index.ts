import { AssetManager } from "./AssetManager";
import { CanvasController } from "./CanvasController";
import { GameController } from "./GameController";

const canvas: HTMLCanvasElement = document.getElementById("main-canvas") as HTMLCanvasElement;
const GAME_WIDTH = 800;
const GAME_HEIGHT = 800;

const canvasController = new CanvasController(canvas, GAME_WIDTH, GAME_HEIGHT);
const assetManager = new AssetManager();

assetManager.loadResource('img/ugdoq-512x512.png', 'ugdoq-sketch');
assetManager.loadResource('img/ugdoq-background.png', 'background');
assetManager.loadResource('img/goblin.png', 'player-spritesheet');

const gameController = new GameController(canvasController, assetManager, GAME_WIDTH, GAME_HEIGHT);

gameController.startGame();

