export class SpriteSheet{

  public animationRows: number
  public animationColumns: number;
  public spriteWidth: number
  public spriteHeight: number

  constructor(public identifier: string, public sheet: HTMLImageElement, numColumns: number, numRows: number){
      this.animationRows = numRows;
      this.animationColumns = numColumns;
      sheet.addEventListener('load', (e) => this.configureSheetDimensions(numColumns, e.target as HTMLImageElement));
  }

  private configureSheetDimensions(columns: number, image: HTMLImageElement){
    this.spriteWidth = image.naturalWidth / columns;
    this.spriteHeight = image.naturalHeight / this.animationRows;
  }
}
