import { SpriteSheet } from "../animation/Animation";
import { Vector2 } from "../movement/Vector";
import { Character } from "./Character";

export class Player implements Character{
  public position: Vector2;
  public state: number;
  public width: number;
  public height: number;
  public currentAnimationFrame = 0;
  public animationResolution = 5;


  private velocity: Vector2;
  private speed = 5;

  // constructor(public spriteSheet: SpriteSheet, public scale: number, private canvasController: CanvasController, x: number, y: number){
  constructor(public spriteSheet: SpriteSheet, public scale: number, x: number, y: number){
    this.position = new Vector2(x, y);
    this.state = 1;
    this.velocity = new Vector2(0, 0);

    spriteSheet.sheet.addEventListener('load', () => this.updateDimensions());
  }

  playerInput(inputs: Vector2){
    this.velocity = inputs;
    this.velocity.scale(this.speed);
  }

  update(): void {

    this.position = this.position.add(this.velocity);
    this.currentAnimationFrame++;
  }

  private updateDimensions(){
    this.width = this.spriteSheet.spriteWidth * this.scale;
    this.height = this.spriteSheet.spriteHeight * this.scale;
  }
}
