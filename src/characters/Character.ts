import { SpriteSheet } from "../animation/Animation";
import { Vector2 } from "../movement/Vector"

export interface Character{
  position: Vector2;
  state: number;
  width: number;
  height: number;
  currentAnimationFrame: number;
  animationResolution: number;
  spriteSheet: SpriteSheet;



  update(): void;
  //  draw(): void;
}
