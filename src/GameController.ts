import { SpriteSheet } from "./animation/Animation";
import { AssetManager } from "./AssetManager";
import { CanvasController } from "./CanvasController";
import { Player } from "./characters/Player";
import { InputController } from "./controls/InputController";
import { DEBUG } from "./environment";
import { ImageResource } from "./ResourceTypes";

export class GameController {
  private gameFrames = 0;
  private lastFrameTime = 0;

  private background: ImageResource;

  private player: Player;
  private inputController: InputController;

  constructor(private canvasController: CanvasController, public assetManager: AssetManager, private gameWidth: number, private gameHeight: number) {
    this.initialize();
  }

  public startGame(){
    const playerSpritesheetImage = (this.assetManager.getResourceByIdentifier('player-spritesheet', 'image') as ImageResource).image;
    const playerSpritesheet = new SpriteSheet('player-spritesheet', playerSpritesheetImage, 11, 5);
    this.player = new Player(playerSpritesheet, 1, 400, 400);
    this.inputController = new InputController(this.player);
    this.gameLoop(0);
  }

  private initialize() {
    console.log("GameController Loaded.");
    const background = this.assetManager.getResourceByIdentifier('background', 'image');
    if (background != null) this.background = background as ImageResource;
  }

  private gameLoop(timeStamp: number){
    const newFrame = timeStamp != this.lastFrameTime;
    const deltaTime = timeStamp - this.lastFrameTime;
    this.lastFrameTime = timeStamp;

    if (newFrame){
      this.canvasController.clearFrame();
      this.canvasController.drawImage(this.background.image, 0, 0, 1);
      this.player.update();
      this.canvasController.drawCharacter(this.player);
      if (DEBUG) this.canvasController.checkFrameRate(deltaTime);
    }

    requestAnimationFrame(this.gameLoop.bind(this));
  }

}

