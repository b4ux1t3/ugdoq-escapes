import { Character } from "./characters/Character";

export class CanvasController{

  private lastSixty: Array<number> = [];
  private ctx: CanvasRenderingContext2D;

  constructor(private canvas: HTMLCanvasElement, private gameWidth: number, private gameHeight: number) {
    this.initialize();
  }

  private initialize(){
    this.canvas.width = this.gameWidth;
    this.canvas.height = this.gameHeight;
    this.ctx = this.canvas.getContext('2d');
  }

  public drawImage(img: HTMLImageElement, dx: number, dy: number, scale = 1) {
    this.ctx.drawImage(img, dx, dy, img.naturalWidth * scale, img.naturalHeight*scale);
  }

  public drawCharacter(character: Character){
    this.ctx.drawImage(
      character.spriteSheet.sheet,
      (Math.floor(character.currentAnimationFrame / character.animationResolution) % character.spriteSheet.animationColumns) * character.spriteSheet.spriteWidth,
      character.state * character.spriteSheet.spriteHeight,
      character.spriteSheet.spriteWidth,
      character.spriteSheet.spriteHeight,
      character.position.x - character.width / 2,
      character.position.y - character.height / 2,
      character.width,
      character.height
    );
  }

  public clearFrame(){
    this.ctx.clearRect(0, 0, this.gameWidth,this.gameHeight);
  }

  public checkFrameRate(deltaTime: number) {
    if (deltaTime === 0) return;
    const framesPerSecond = 1 / deltaTime * 1000;

    if (this.lastSixty.length === 60) {
      this.lastSixty = [...this.lastSixty.slice(1), framesPerSecond];
    } else {
      this.lastSixty.push(framesPerSecond);
    }
    const output = `${(this.lastSixty.reduce((x, y) => x + y) / this.lastSixty.length).toFixed(2)}`;
    this.ctx.fillStyle = 'white';
    const dimensions = this.ctx.measureText(output);
    this.ctx.fillText(output, this.canvas.width - 20 - dimensions.width, 20);

  }
}
