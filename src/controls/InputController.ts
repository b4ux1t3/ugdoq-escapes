import { Player } from "../characters/Player";
import { Vector2 } from "../movement/Vector";

export class InputController{

  private up = 0;
  private down = 0;
  private left = 0;
  private right = 0;

  constructor(private player: Player){
    this.registerControls();
  }

  private registerControls(){
    document.addEventListener('keydown', this.checkKeyDown.bind(this));

    document.addEventListener('keyup', this.checkKeyUp.bind(this));
  }

  private checkKeyDown(e: KeyboardEvent) {
    switch(e.key){
      case 'w':
        this.up = 1;
        break;
      case 's':
        this.down = 1;
        break;
      case 'a':
        this.left = 1;
        break;
      case 'd':
        this.right = 1;
        break;
    }
    this.player.playerInput(this.calcMoveVector());
  }

  private checkKeyUp(e: KeyboardEvent) {
    switch(e.key){
      case 'w':
        this.up = 0;
        break;
      case 's':
        this.down = 0;
        break;
      case 'a':
        this.left = 0;
        break;
      case 'd':
        this.right = 0;
        break;
    }
    this.player.playerInput(this.calcMoveVector())

  }

  private calcMoveVector(): Vector2{
    const horizontalMovement = this.right + -this.left;
    const verticalMovement = this.down + -this.up;

    const newVec = new Vector2(horizontalMovement, verticalMovement);
    newVec.normalize();
    return newVec;
  }
}
