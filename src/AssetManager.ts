import { ImageResource, Resource, ResourceType } from "./ResourceTypes";

export class AssetManager {

  imageResources: Array<ImageResource> = [];

  public loadResource(path: string, identifier: string, resType: ResourceType = 'image'){
    switch (resType){
      case 'image':
        this.addImage(path, identifier);

    }
  }
  public getResourceByIdentifier(identifier: string, resType: ResourceType): Resource | null{
    switch(resType){
      case 'image':
        return this.getImageResourceByIdentifier(identifier);
    }
  }
  private addImage(path: string, identifier: string){
    const image = new Image();
    image.src = path;
    this.imageResources.push({image, identifier});
  }

  private getImageResourceByIdentifier(identifier: string): ImageResource | null{
    const imageSearch = this.imageResources.filter(res => res.identifier === identifier);

    if (imageSearch.length > 0) return imageSearch[0];

    return null;
  }
}
