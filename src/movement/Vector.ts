export class Vector2 {
  public get magnitude(): number {
    return Math.sqrt((this.x * this.x) + (this.y * this.y));
  }

  constructor(public x: number, public y: number){
  }


  public normalize(){
    if (this.magnitude != 0) this.scale(1 / this.magnitude);
  }

  public multiply(magnitude: number): Vector2{
    return new Vector2(
      this.x * magnitude,
      this.y * magnitude
    );
  }

  public scale(magnitude: number) {
    this.x *= magnitude;
    this.y *= magnitude;
  }

  public add(vec: Vector2): Vector2{
    return  new Vector2 (
      this.x + vec.x,
      this.y + vec.y
    )
  }

  public distance(vec: Vector2): number {
    return vec.add(this.multiply(-1)).magnitude;
  }
}
