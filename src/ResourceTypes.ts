export type ResourceType =
  | 'image'
  | 'text'
  | 'audio'
  | 'blob'

export type Resource =
  | ImageResource
  | TextResource
  | AudioResource
  | BlobResource


export type ImageResource ={
  image: HTMLImageElement,
  identifier: string,
}

export type TextResource = {
  text: string,
  identifier: string,
}

export type AudioResource = {
  audio: HTMLAudioElement,
  length: number,
  identifier: string,

}

export type BlobResource = {
  data: unknown,
  identifier: string,
}
